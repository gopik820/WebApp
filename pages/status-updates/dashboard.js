import React, { useEffect, useState } from 'react';

import Base from '../../components/base';
import TitleBar from '../../components/titlebar';
import Dashboard from '../../modules/statusUpdates/components/Statusdashboard';


const StatusupdateDashboard = (props) => {
    const routes = [
      {
        path: '/',
        name: 'Home',
      },
      {
        path: '/status-update',
        name: 'Status Update',
      },
      {
        path: '/status-update/dashboard',
        name: 'Dashboard',
      },
    ];
    return (
        <div className="app">
        <Base title="Dashboard">
          <TitleBar title="Dashboard" routes={routes} />
          <div className="row m-0">
          <Dashboard />
          </div>
        </Base>
      </div>
    );
  };
  
export default StatusupdateDashboard;